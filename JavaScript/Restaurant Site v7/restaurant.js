

//	Requirements:
//	1.  When the user submits the Contact Us form, use JavaScript to check:
//		a. Name and one form of contact information (Email or Phone) should be filled in.
//		b. If Reason for Inquiry's dropdown is selected to Other, make sure that the Additional Information is filled in.
//		c. Best days to contact you must have at least one day checked.
//	2. If any of these are blank, display a pop-up to politely remind the user to fill in these fields.

function validateForm() {
    var contactName = document.forms["contactSmokin"]["usr"].value;
    var alertName;
    var contactEmail = document.forms["contactSmokin"]["email"].value;
    var alertEmail;
    var contactPhone = document.forms["contactSmokin"]["phone"].value;
    var alertPhone;
    var contactReason = document.forms["contactSmokin"]["inquiryReason"].value;
    var alertReason;
    var contactMoreInfo = document.forms["contactSmokin"]["additionalInfo"].value;
    var alertMoreInfo;
    var contactQuestion = document.forms["contactSmokin"]["question"].value;
    var alertQuestion;
    var contactAvailable = document.forms["contactSmokin"]["available"].value;
    var alertAvailable;

    if (contactName == null || contactName == "") {
       alert("Please provide your name.");
       alertName = true;
       console.log("Alert Name is "+alertName);
       }

    if ((contactEmail == null || contactEmail == "") && (contactPhone == null || contactPhone == "")) {
       alert("Please provide an email address or a phone number.");
       alertEmail = true;
       alertPhone = true;
       console.log("Alert Email & Alert Phone is "+alertEmail+" and "+alertPhone);
       }

    document.getElementById("inquiryReason").selectedIndex;
      console.log("Reason Selected is "+inquiryReason.value);
      if (inquiryReason.value == "Other") {
//		alert("You selected 'Other.' Please provide more detail about your inquiry so that we can better serve you.");
	  }

    if (contactMoreInfo.length == 0 && inquiryReason.value == "Other"){
        alert("You selected 'Other.' Please provide more detail about your inquiry so that we can better serve you.");
	}
    else if (contactMoreInfo.length == 0){
	    alertMoreInfo = true;
	    alert("Don't forget to provide more details.");
	    console.log("Additional Information is empty.");}
	else{
		alertMoreInfo = false;
	    console.log("Additional Information is "+contactMoreInfo);
	}

//    document.getElementById(document.forms["contactSmokin"]["additionalInfo"]);
//	  console.log("Additional Information is "+contactMoreInfo);
//	  if (additionalInfo.value == null){
//	    alert("Don't forget to provide more details.");
//	}

    if (contactQuestion == null || contactQuestion == "") {
       alert("Have you been to our restaurant?");
       alertQuestion = true;
       console.log("Question unanswered.");}
    else{
	   alertQuestion = false;
       console.log("Question answered.");
       }

	var cbResults = 'Checkboxes: ';
    var radioResults = 'Radio buttons: ';
    for (var i = 0; i < contactSmokin.elements.length; i++ ) {
        if (contactSmokin.elements[i].type == 'checkbox') {
            if (contactSmokin.elements[i].checked == true) {
                cbResults += contactSmokin.elements[i].value + ' ';
//                alert("Checkbox values "+cbResults);
                console.log("Checkbox values "+cbResults);
            }
        }
        if (contactSmokin.elements[i].type == 'radio') {
            if (contactSmokin.elements[i].checked == true) {
                radioResults += contactSmokin.elements[i].value + ' ';
//                alert("Radio Button values "+radioResults);
                console.log("Radio Button values "+radioResults);
            }
        }
}




	  alert("NAME: "+usr.value+" EMAIL: "+email.value+" PHONE: "+phone.value+" REASON FOR INQUIRY: "+inquiryReason.value+" ADDITIONAL INFO: "+additionalInfo.value+" VISITED RESTAURANT: "+radioResults+" AVAILABILITY: "+cbResults);
}